package agenda;

import minha_data.Data;

import java.time.LocalDate;

public class Contato {

    private String nome;
    private String telefone;
    private String email;
    private Data nascimento;

    public Contato(String umNome, String umTelefone, String umEmail) {
        nome = umNome;
        setTelefone(umTelefone);
        setEmail(umEmail);
    }

    public void setTelefone(String telefonePossivel) {
        if (!telefonePossivel.isEmpty()) {
            telefone = telefonePossivel;
        }
    }

    public void setEmail(String emailPossivel) {
        if (!emailPossivel.isEmpty() && emailPossivel.contains("@")) {
            email = emailPossivel;
        }
    }
}
