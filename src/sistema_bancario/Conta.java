package sistema_bancario;

public class Conta {

    public int numero;
    public Correntista Correntista;
    public double saldo;
    public double limiteDeSaldo;


    public Conta() {
        saldo = 0;
        limiteDeSaldo = 200;
    }
    public Conta(int _saldo, int _limiteSaldo) {
        saldo = _saldo;
        limiteDeSaldo = _limiteSaldo;
        DuplicarLimiteDeSaldo();
    }
    public Conta(int numeroDaConta) {
        numero = numeroDaConta;
    }
    public String getNameCorrentista() {
        return Correntista.nome;
    }
    private void DuplicarLimiteDeSaldo() {
        limiteDeSaldo = 2 * limiteDeSaldo;
    }
}
