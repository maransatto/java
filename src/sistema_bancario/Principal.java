package sistema_bancario;

import calculadora.Calculadora;

import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

    public static ArrayList<Conta> listaDeContas;
    public static Conta conta;
    public static String opcao;

    public static void main(String[] args) {

        listaDeContas = new ArrayList<Conta>();
        conta = null;

        DemonstrarOpcoes();

        do {

            // Opção selecionada na tela inicial
            switch (opcao) {
                case "n":
                    AdicionarConta();
                    break;
                case "a":
                    AcessarContaExistente();
                    break;
                case "t":
                    TrocarCorrentista();
                    break;
                case "l":
                    ListarCorrentistas();
                    break;
                case "v":
                    VerSaldo();
                    break;
                case "d":
                    Depositar();
                    break;
                case "s":
                    Sacar();
                    break;
                default:
                    if (!opcao.equals("x")){
                        System.out.println("Opção invalida!");
                    }
            }

        } while (!opcao.equals("x"));

    }
    protected static void DemonstrarOpcoes() {
        Scanner entrada = new Scanner(System.in);

        System.out.println("***************************************************");
        System.out.println("***************** Banco Maransatto ****************");
        System.out.println("***************************************************");

        if (conta == null) {
            System.out.print("Escolha a opção Desejada: ");
            System.out.println("(n)ova conta | (a)cessar conta existente | (l)istar correntistas  | (x)exit");
        }
        else {
            System.out.println("Bem vindo " + conta.Correntista.nome + "!");
            System.out.print("Escolha a opção Desejada: ");
            System.out.println("(v)ver saldo | (d)epositar | (s)acar | (t)rocar Correntista | (x)exit");
        }
        opcao = entrada.next();
    }
    protected static void AdicionarConta() {
        Scanner entrada = new Scanner(System.in);

        System.out.println("Informe o nome do Correntista");

        Correntista correntista = new Correntista();
        correntista.nome = entrada.next();

        Conta _conta = new Conta(100, 200);
        System.out.println(_conta.limiteDeSaldo);
        _conta.Correntista = correntista;

        listaDeContas.add(_conta);
        conta = _conta;

        System.out.println("Correntista adicionado com sucesso!\n");

        DemonstrarOpcoes();
    }
    protected static void ListarCorrentistas() {

        if (!listaDeContas.isEmpty()) {
            for(Conta c : listaDeContas) {
                System.out.println(c.Correntista.nome);
            }
            System.out.print("\n");
        }
        else {
            System.out.println("Nenhum\n");
        }

    }
    protected static void AcessarContaExistente() {

        Scanner entrada = new Scanner(System.in);

        System.out.println("Informe o nome do Correntista");

        // incluir tratamento pra ver se realmente existe
        String busca = entrada.next();
        // Busca o correntista pelo nome
        conta = listaDeContas.stream().filter(x -> x.Correntista.nome.equals(busca)).findFirst().get();
    }
    protected static void TrocarCorrentista() {
        conta = null;
        DemonstrarOpcoes();
    }
    protected static void VerSaldo() {
        System.out.println(conta.saldo);
    }
    protected static void Depositar() {

        Calculadora calc = new Calculadora(conta.saldo);
        Scanner entrada = new Scanner(System.in);

        System.out.print("Informe o Valor: ");

        double valor = Double.parseDouble(entrada.nextLine());

        if ((conta.saldo + valor) > conta.limiteDeSaldo) {
            System.out.println("Limite máximo de saldo atingido!");
        }
        else {
            calc.somar(entrada.nextDouble());
            conta.saldo = calc.operando;
        }
    }
    protected static void Sacar() {
        Calculadora calc = new Calculadora(conta.saldo);
        Scanner entrada = new Scanner(System.in);

        System.out.print("Informe o Valor: ");

        double valor = Double.parseDouble(entrada.nextLine());

        if ((conta.saldo - valor) < 0) {
            System.out.println("Você não tem cheque especial, se liga!");
        }
        else {
            calc.subtrair(entrada.nextDouble());
            conta.saldo = calc.operando;
        }
    }
}
