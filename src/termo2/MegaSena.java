package termo2;

import java.util.Arrays;
import java.util.Random;

public class MegaSena {

    public static void main(String args[]) {

        int[] numeros = new int[6];

        for (int i = 0; i < 6; i++) {

            numeros[i] = numeroRandomico();

            for (int x = i-1; x > 0; x--) {

                if (numeros[x] == numeros[i]) {
                    i--;
                    break;
                }
            }
        }


        // sorting array
        Arrays.sort(numeros);

        // let us print all the elements available in list
        System.out.println("Este e o resultado parcial da Tele Sena:");

        for (int number : numeros) {
            System.out.println(number);
        }
    }

    public static int numeroRandomico(){
        Random rand = new Random();
        int randomNum = rand.nextInt(60) + 1;

        return randomNum;
    }
}
