package minha_data;

import javax.swing.*;

public class Principal {
    public static void main(String[] args) {

        try {

            Data hoje = new Data();
            String dia = JOptionPane.showInputDialog(null, "Digite o dia: ");
            hoje.setDia(Integer.parseInt(dia));
            String mes = JOptionPane.showInputDialog(null, "Digite o mes: ");
            hoje.setMes(Integer.parseInt(mes));
            String ano = JOptionPane.showInputDialog(null, "Digite o ano: ");
            hoje.setAno(Integer.parseInt(ano));
            JOptionPane.showMessageDialog(null, "Dia: " + hoje);

        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "Deu ruim mano, digita certo ae!");
        }
    }
}
