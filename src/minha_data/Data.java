package minha_data;

public class Data {
    private int dia, mes, ano;

    public Data() {
        dia = 1;
        mes = 1;
        ano = 1900;
    }
    public Data(int umDia, int umMes, int umAno) {
        setDia(umDia);
        setMes(umMes);
        setAno(umAno);
    }

    public String toString() {
        return dia + "/" + mes + "/" + ano;
    }

    public void setDia(int diaPossivel) {
        if (diaPossivel >= 1 && diaPossivel <= 31) {
            dia = diaPossivel;
        }
    }
    public void setMes(int mesPossivel) {
        if (mesPossivel >= 1 && mesPossivel <= 12) {
            mes = mesPossivel;
        }
    }
    public void setAno(int anoPossivel) {
        if (anoPossivel >= 1900 && anoPossivel <= 2100) {
            ano = anoPossivel;
        }
    }
    public int getDia() {
        return dia;
    }
    public int getMes() {
        return mes;
    }
    public int getAno() {
        return ano;
    }
}
