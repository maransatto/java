package calculadora;

import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        Calculadora calc = new Calculadora();

        String opcao = "";

        do {

            System.out.println("= " + calc.operando);
            System.out.print("Opçoes: (+) (-) (*) (/) (r)aiz (p)otencia (l)impar (s)air");
            System.out.println("? ");
            opcao = entrada.next();

            switch (opcao) {
                case "+":
                    calc.somar(entrada.nextDouble());
                    break;
                case "-":
                    calc.subtrair(entrada.nextDouble());
                    break;
                case "*":
                    calc.multiplicar(entrada.nextDouble());
                    break;
                case "/":
                    calc.dividir(entrada.nextDouble());
                    break;
                case "r":
                    calc.raizQuadrada();
                    break;
                case "p":
                    calc.potencia(entrada.nextDouble());
                    break;
                case "l":
                    calc.limpar();
                    break;
                default:
                    if (!opcao.equals("s")){
                        System.out.println("Opçao invalida!");
                    }
            }

        } while (!opcao.equals("s"));
        entrada.close();
    }
}
