package calculadora;

public class Calculadora {

    public double operando;

    public Calculadora() {
        operando = 0;
    }
    public Calculadora(double valorInicial) {
        operando = valorInicial;
    }
    public void somar(double valor) {
        operando += valor;
    }
    public void subtrair(double valor) {
        operando -= valor;
    }
    public void multiplicar(double valor) {
        operando *= valor;
    }
    public void dividir(double valor) {
        operando /= valor;
    }
    public void limpar() {
        operando = 0;
    }
    public void raizQuadrada() {
        operando = Math.sqrt(operando);
    }
    public void potencia(double valor) {
        operando = Math.pow(operando, valor);
    }
}