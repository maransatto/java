package vendas;

import org.omg.IOP.TAG_CODE_SETS;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class Cliente {

    public int id;
    public String nome;
    public LocalDate nascimento;

    public Cliente() {
        id = 0;
        nome = "NÃO INFORMADO";
        nascimento = LocalDate.of(1900, Month.JANUARY, 1);
    }
    public Cliente(int _id, String _nome, LocalDate _nascimento) {
        id = _id;
        nome = _nome;
        nascimento = _nascimento;
    }

    public String toString() {
        String fichaDoCliente = "#########\nCódigo: " + id + "\nNome: " + nome + "\nNascimento; " + nascimento;
        return fichaDoCliente;
    }

    public int calcularIdade() {
        LocalDate hoje = LocalDate.now();
        long idade = ChronoUnit.YEARS.between(nascimento, hoje);
        return (int)idade;
    }

}
