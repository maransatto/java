package vendas;

import java.time.LocalDate;
import java.time.Month;

public class Principal {

    public static void main(String[] args) {

        Cliente cliente = new Cliente(1, "Maransatto", LocalDate.of(1988, Month.MAY, 19));
        System.out.println("O cliente " + cliente.nome + " tem " + cliente.calcularIdade() + " anos.");

        Cliente cliente2 = new Cliente();
        cliente2.id = 1;
        cliente2.nome = "Derci";
        cliente2.nascimento = LocalDate.of(1451, Month.FEBRUARY, 10);

        System.out.println("O cliente " + cliente2.nome + " tem " + cliente2.calcularIdade() + " anos.");

        Cliente cliente3 = new Cliente();
        cliente3.nome    = "terraplanista";
        System.out.println("O cliente " + cliente3.nome + " tem " + cliente3.calcularIdade() + " anos.");

        Produto p1 = new Produto(1, "O de Ovo", 2.50);
        Produto p2 = new Produto(2, "R de Rato", 2.50);
        Produto p3 = new Produto(3, "M de Macaco", 2.50);

        System.out.println(p1 + "\n" + p2 + "\n" + p3);
    }
}
