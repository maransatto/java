package vendas;

public class Produto {

    public int id;
    public String descricao;
    public double preco;

    public Produto(int _id, String _descricao, double _preco) {
        id = _id;
        descricao = _descricao;
        preco = _preco;
    }

    public String toString() {
        String fichaDoProduto = "#########\nCódigo: " + id + "\nDescricao: " + descricao + "\nPreço; " + preco;
        return fichaDoProduto;
    }

}
